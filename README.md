## Resources/Explanations:
* For more information on how PKI keys work for signing code, see [DIGICERT's nice explanation](https://www.digicert.com/signing/code-signing-certificates). Additionally, they provide services to act as a CA.
* See the [Wikipedia explanation of PKI](https://en.wikipedia.org/wiki/Public_key_infrastructure#Certificate_authorities) as a certificate authority.
* See the `NXP CST (Code Signing Tool) User's Guide` located in the CST download at `<code_signing_tool>/docs/CST_UG.pdf`. It explains pretty much everything in more detail. VERY GOOD!
* See the [NXP AN4581.pdf](https://www.nxp.com/docs/en/application-note/AN4581.pdf) document for more HAB4 information (must have an NXP account).
* [HAB4 Documentation in U-Boot](https://github.com/u-boot/u-boot/blob/master/doc/imx/habv4/introduction_habv4.txt) (very good)
* A pretty [set of slides showing the various HAB procedures](https://community.nxp.com/pwmxy87654/attachments/pwmxy87654/connects/370/1/AMF-AUT-T2794.pdf)

## Generate Keys

1. Download the [CST (Code Signing Tool)](https://www.nxp.com/webapp/sps/download/license.jsp?colCode=IMX_CST_TOOL) from NXP (you will need an account). Extract the archive.
1. `cd <code_signing_tool>/keys/`
1. Note: HAB4 is the version of high assurance boot that is used by the imx8mp. So everything in this document is specific to HAB4.
1. Ensure you have OpenSSL install by running `openssl version`.
1. Create file key_pass.txt with the password for the keys (whether they are existing to be created). The CST tool using OpenSSL to generate the keys and do the binary signing and it requires the **password be at least 4 character long**.  
    The password needs to be duplicated on line 2 as well as line 1 in the text file:
    **DO NOT LOSE THE PASSWORD**
    ```
    PASS="password"
    echo "$PASS"  > key_pass.txt
    echo "$PASS" >> key_pass.txt
    ```
    **DO NOT LOSE THE PASSWORD. If you do, you will not be able to sign binaries for devices whose key has already been fused and you will no longer be able to use the private signing keys generated.**
1. [Optional if using existing keys]: Create file `serial` containing an 8-digit code which represents the certificate serial number.
    ```
    SERIAL_NUM="12481632"
    echo "$SERIAL_NUM" > serial
    ```
2. Create the [PKI](https://en.wikipedia.org/wiki/Public_key_infrastructure#Certificate_authorities) tree for HAB4: `./hab4_pki_tree.sh`.  
    You can use the following as default answers to the prompts:
    ```
    Do you want to use an existing CA key (y/n)?: n
    Do you want to use Elliptic Curve Cryptography (y/n)?: n
    Enter key length in bits for PKI tree: 4096
    Enter PKI tree duration (years): 20
    How many Super Root Keys should be generated? 4
    Do you want the SRK certificates to have the CA flag set? (y/n)?: y
    ```
    * **DO NOT LOSE THE GENERATED KEYS: YOU MUST KEEP THE `./certs` AND `./keys/` DIRECTORIES. YOU WILL NOT BE ABLE TO FLASH THE DEVICE EVER AGAIN IF YOU FUSED AND LOSE THESE KEYS. ALSO THE PASSWORD MUST BE SAVED OR YOU WILL NOT BE ABLE TO EVER FLASH AGAIN**

    * **NOTE**: if you use a key length other than `4096`, the key-file names in yocto will need to be updated.
    * **NOTE**: if you generate not equal to 4 Super Root Keys, key-file names in yocto will need to be updated.

    * You can refer to the CST User's Guide listed in the resources section for more details.

    * This will generate the [SRK], [CSF] and [IMG] keys and certificates in the `./keys` and `./crts` directory.

    * The generated keys will exist in PKCS#8 format in both PEM and DER forms.

    * Certificates are located in the `./crts` directory and are in [X.509](https://en.wikipedia.org/wiki/X.509) format in both PEM and DER format.

    * The `cst` will accept key and certificate files in either PEM or DER form.
    * `Do you want to use an existing CA key (y/n)?:`  
        * Choose no  if you do **not** have a CA key. One will be created.
        * Choose yes if you do have a CA key. You will be prompted to provide the filenames (path) to the CA key and CA public key (certificate).
    * `Do you want to use Elliptic Curve Cryptograph (y/n)?`
        * See this nice page about [RSA vs ECC](https://www.ssl2buy.com/wiki/rsa-vs-ecc-which-is-better-algorithm-for-security)
        * — If “n”, RSA is used.
        * — If “y”, ECC is used.
    * `Enter key length ... for PKI tree:`:  
        * If RSA:  
            * Enter RSA key length in bits for PKI tree:  
          – This is the length in bits for the RSA keys in the tree. For HAB4 1024, 2048, 3072 and 4096-bit RSA keys are supported. All keys in the tree are generated with the same length.
        * If ECC: 
            * Enter length for elliptic curve to be used for PKI tree: Possible values p256, p384, p521:  
            – This is the length in bit for the keys in the tree. For HAB4 P256, P384 and P521 EC keys are supported. All keys in the tree are generated with the same length.
    * `Enter PKI tree duration (years):`  
          This defines the validity period of the corresponding certificates.
    * `How many Super Root Keys should be generated?`  
        * One to four SRKs may be generated. (1-4).
        * Technically, at least 1 additional key can be created (imx8mp fuses should physically support up to 5 keys) manually. Refer to the documentation if you really want an extra key(s).
        * The reason to use the recommended 4 keys is because keys can be revoked. If, forsay, one of the keys is comprimised, the fuses for that key can be fused on the imx8 to prevent it from ever being used for authentication. This can be done for up to 3 of the 4 keys, but at least one is required.  
            Additionally, the yocto recipe will need to be updated in that case. The CSF dictates which key to use for authentication. Only one key can be used per reset cycle. See the section below `Yocto/CSF Key Selection Note`
    * `Do you want the SRK certificates to have the CA flag set? (y/n)?`  
         Answer ‘y’ for a standard tree, ‘n’ for fast authentication tree. Fast authentication means that the only the certificate keys are needed. The certificate keys will be used directly for authentication.  
          This is poorly worded. It should say something like "Do you want to use fast authentication by only using (SRK) certificate (public) keys?".  
        * See the image in the [PKI Trees section](#pki-trees:)

3. Generate HAB4 SRK (Super Root Key) Table and corresponding hash values to fuse to imx8.  
    Creates `SRK_*_table.bin` & `SRK_*_fuse.bin` where `*` is the sequence of numbers with length equal to the number of keys. In the example above we created 4 keys, therefore, the table and fuse hash we create will use all 4 keys in this example:
    ```
    cd ../crts/
    ../linux64/bin/srktool -h 4 -t SRK_1_2_3_4_table.bin -e SRK_1_2_3_4_fuse.bin -d sha256 -c ./SRK1_sha256_4096_65537_v3_ca_crt.pem,./SRK2_sha256_4096_65537_v3_ca_crt.pem,./SRK3_sha256_4096_65537_v3_ca_crt.pem,./SRK4_sha256_4096_65537_v3_ca_crt.pem -f 1
    ```
4. Save the contents of the `./crts` and `./keys` dirs somewhere safe (like a repo) (will be used by yocto) and make sure to delete `./keys/serial` and `./keys/key_pass.txt` which are plaintext.

#### Yocto/CSF Key Selection Note:
At build time, exactly one key must be specified to be used for authentication. It is specified in the CSF script and used when signing the binaries. See page 24 of the CST User's Guide.

The folling Install SRK entry in the CSF file is what dictates which authentication key to use as the `Source Index` variable. Valid values are 0-3 and represent the keys 1-4 respectively:
```
[Install SRK] # HAB4 example
File = “../crts/srk_table.bin”
Source Index = 0
Hash Algorithm = sha256
```
#### PKI Trees:

Standard vs fast authentication trees:

![standard-fast-authentication](standard-fast-authentication.png)

A nice image from the CST User's Guide:
![PKI Tree and Keys](pki-tree-info.png)
